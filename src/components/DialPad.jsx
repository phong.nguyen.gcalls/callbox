import '../styles/DialPad.css'
import { useEffect } from 'react'
import { StartCall } from './index';

export function DialPad({ data, phoneNumber, setPhoneNumber, isCalling, setIsCalling }) {

    const handleSetNumber = (e) => {
        const value = e.target.value;
        if (phoneNumber.length < 10) {
            setPhoneNumber(value);

        }
    }

    const handleKeyDown = (event) => {
        if (event.keyCode === 8) {
            // Người dùng đã nhấn nút "Back" trên bàn phím (mã phím 8)
            setPhoneNumber((pre) => {
                return pre.slice(0, pre.length - 1)
            }) // Xóa dữ liệu trên input
        }
    };

    useEffect(() => {
        document.addEventListener('keydown', handleKeyDown);

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
        };
    }, []);

    const handleKeyDownInput = (event) => {
        const key = event.key;

        // Kiểm tra nếu phím nhấn không phải là số hoặc các ký tự đặc biệt
        if (!/^[0-9*#]$/.test(key)) {
            event.preventDefault(); // Ngăn chặn sự kiện mặc định (không cho nhập ký tự)
        }
    };

    const handlePaste = (event) => {
        const pastedData = event.clipboardData.getData('text/plain');

        // Kiểm tra nếu dữ liệu dán vào chỉ chứa số, * và #
        if (!/^[0-9*#]*$/.test(pastedData)) {
            event.preventDefault(); // Ngăn chặn sự kiện mặc định (không cho dán ký tự)
        }
    };

    return (

        <div className="container grid flex justify-center">
            <div>
                <div className='flex justify-between text-white'>
                    <input
                        type='text'
                        className='bg-transparent text-white text-3xl outline-none'
                        value={phoneNumber}
                        onChange={(e) => handleSetNumber(e)}
                        onKeyDown={handleKeyDownInput}
                        onPaste={handlePaste}
                        autoFocus />
                    <i
                        onClick={() => setPhoneNumber((pre) => {
                            return pre.slice(0, pre.length - 1)
                        })}
                        className="fa-solid fa-delete-left text-white text-3xl ml-3 mb-1 cursor-pointer"></i>
                </div>
                <hr />
            </div>

            <div className="grid grid-cols-3 gap-4 text-white text-4xl max-w-lg w-96">
                {
                    data.map((number) => (
                        <div
                            key={number}
                            className="cursor-pointer py-6"
                            onClick={() => setPhoneNumber((pre) => {
                                if (pre)
                                    return [...pre, number].join('').slice(0, 10);
                                else return [number].join('');
                            })}
                        >{number}</div>)
                    )
                }
            </div>

            <button
                className="bg-green-500 hover:bg-green-700 text-white text-3xl font-bold py-2 px-36 rounded-3xl shadow-2xl mt-8"
                onClick={() => {
                    if (phoneNumber.length === 10) {
                        StartCall(phoneNumber, isCalling, setIsCalling);
                        setIsCalling(!isCalling);
                    }
                }}
            >
                <i className="fa-solid fa-phone"></i>
            </button>
        </div>
    )
}
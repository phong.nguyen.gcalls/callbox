
import { StopCall, ToggleMute } from './index';

export function Phoning({ phoneNumber, isCalling, setIsCalling, isMute, setIsMute }) {
    return (
        <div>
            <div className='flex flex-col justify-center text-white'>
                <p className='text-3xl bg-transparent text-center block'>{phoneNumber}</p>
                <span>Gcalls</span>
                <span className='pt-10'>Đang gọi...</span>
            </div>

            <div className='flex justify-between text-white py-40'>
                <div
                    style={{ borderColor: isMute ? 'red' : 'white', color: isMute ? 'red' : 'white' }}
                    className='flex border-solid border-2 rounded-full text-xl justify-center items-center w-16 h-16 mx-2 cursor-pointer'
                    onClick={() => ToggleMute(setIsMute, isMute)}
                >
                    <i className="fa-solid fa-microphone-slash"></i>
                </div>
                <div className='flex border-solid border-2 rounded-full text-xl justify-center items-center w-16 h-16 mx-2 cursor-pointer'>
                    <i className="fa-regular fa-keyboard"></i>
                </div>
                <div className='flex border-solid border-2 rounded-full text-xl justify-center items-center w-16 h-16 mx-2 cursor-pointer'>
                    <i className="fa-solid fa-pause"></i>
                </div>
                <div className='flex border-solid border-2 rounded-full text-xl justify-center items-center w-16 h-16 mx-2 cursor-pointer'>
                    <i className="fa-solid fa-phone-volume"></i>
                </div>
            </div>

            <button
                className="bg-red-500 hover:bg-red-700 text-white text-2xl font-bold py-2 px-8 rounded-3xl shadow-2xl mt-8"
                onClick={() => {
                    StopCall();
                    setIsCalling(!isCalling);
                }}
            >
                <i className="fa-solid fa-phone-slash"></i>
            </button>
        </div>
    )
}